<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RestaurantAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\Category'
            ])
            ->add('description')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\Category'
            ])
            ->add('description', TextareaType::class)
            ->add('imageFile', FileType::class);
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\Category'
            ])
            ->add('description')
        ;
    }
}
