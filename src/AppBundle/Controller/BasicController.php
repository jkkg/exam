<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Review;
use AppBundle\Form\CommentType;
use AppBundle\Form\ImageType;
use AppBundle\Form\RemoveCommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $restaurants = $this->getDoctrine()->getRepository('AppBundle:Restaurant')->findAll();

        return $this->render('AppBundle:Basic:index.html.twig', array(
            'restaurants' => $restaurants
        ));
    }


    /**
     * @Route("/restaurants/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newRestaurantAction(Request $request)
    {

        /** @var Restaurant $restaurant */
        $restaurant = new Restaurant();

        $form_builder = $this->createFormBuilder($restaurant);
        $form_builder->add('title', TextType::class);
        $form_builder->add('category', EntityType::class, array(
           'class' => 'AppBundle\Entity\Category'
        ));
        $form_builder->add('description', TextareaType::class);
        $form_builder->add('imageFile', FileType::class);
        $form_builder->add('save', SubmitType::class, array('label' => 'Submit new place'));

        $form = $form_builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $restaurant = $form->getData();
            $restaurant->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('app_basic_profilerestaurant', array(
                'restaurant_id' => $restaurant->getId()
            ));
        }

        return $this->render('AppBundle:Basic:new_restaurant.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/restaurants/{restaurant_id}")
     * @param int $restaurant_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileRestaurantAction(int $restaurant_id, Request $request)
    {
        $restaurant = $this->getDoctrine()->getRepository('AppBundle:Restaurant')->find($restaurant_id);

        $reviews = $restaurant->getReviews();

        $deleteComment = [];

        foreach ($reviews as $review) {
            $deleteComment[$review->getId()] = $this->createForm(RemoveCommentType::class,null,[
                'method' => 'DELETE',
                'action' => $this->generateUrl('app_basic_delete', ['id'=> $review->getId()])
            ])->createView();
        }

        /** @var Review $review */
        $review = new Review();
        $review_form = $this->createForm(CommentType::class, $review);
        $review_form->handleRequest($request);

        $review_form_twig = $review_form->createView();

        foreach ($reviews as $review) {
            if ($review->getUser() == $this->getUser()) {
                $review_form_twig = null;
            }
        }

        if ($review_form->isSubmitted() && $review_form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $review->setUser($this->getUser());
            $review->setRestaurant($restaurant);
            $review->setPublishDate(new \DateTime());

            $em->persist($review);
            $em->flush();
        }

        $image = new Image();
        $image_form = $this->createForm(ImageType::class, $image);
        $image_form->handleRequest($request);

        if ($image_form->isSubmitted() && $image_form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $image->setUser($this->getUser());
            $image->setRestaurant($restaurant);

            $em->persist($image);
            $em->flush();
        }

        $images = $restaurant->getImages();
        return $this->render('AppBundle:Basic:restaurant_profile.html.twig', array(
            'restaurant' => $restaurant,
            'images' => $images,
            'reviews' => $reviews,
            'review_form' => $review_form_twig,
            'image_form' => $image_form->createView(),
            'delete_comment' => $deleteComment
        ));
    }

    /**
     * @Route("/search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function searchAction(Request $request) {
        $data = $request->request->get(trim('data'));

        $restaurants = $this->getDoctrine()->getRepository('AppBundle:Restaurant')->findRestaurant($data);

        return $this->render('AppBundle:Basic:search_results.html.twig', array(
            'restaurants' => $restaurants
        ));
    }

    /**
     * @Route("/review/{id}", requirements={"id": "\d+"})
     * @Method({"DELETE"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(int $id)
    {
        $review = $this->getDoctrine()
            ->getRepository('AppBundle:Review')
            ->find($id);

        $id = $review->getRestaurant()->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($review);
        $em->flush();
        return $this->redirectToRoute("app_basic_profilerestaurant", [
            'restaurant_id' => $id]);
    }

}
