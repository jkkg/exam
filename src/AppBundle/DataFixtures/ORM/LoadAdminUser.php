<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAdminUser implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();

        $admin
            ->setEmail('admin@admin.ru')
            ->setUsername('admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password1 = $encoder->encodePassword($admin, '123');
        $admin->setPassword($password1);
        $manager->persist($admin);

        $manager->flush();
    }
}