<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategory extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category2 = new Category();
        $category3 = new Category();
        $category4 = new Category();
        $category5 = new Category();

        $category1->setTitle('Ресторан');
        $category2->setTitle('Кафе');
        $category3->setTitle('Кофейня');
        $category4->setTitle('Таверна');
        $category5->setTitle('Паб');

        $manager->persist($category1);
        $manager->persist($category2);
        $manager->persist($category3);
        $manager->persist($category4);
        $manager->persist($category5);

        $manager->flush();
    }
}