<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUsers extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'username' => 'user0',
                'email' => 'doctor0@ya.ru',
            ],
            [
                'username' => 'user1',
                'email' => 'doctor1@ya.ru',
            ],
            [
                'username' => 'user2',
                'email' => 'doctor2@ya.ru',
            ],
            [
                'username' => 'user3',
                'email' => 'doctor3@ya.ru',
            ],
            [
                'username' => 'user4',
                'email' => 'doctor4@ya.ru',
            ],
            [
                'username' => 'user5',
                'email' => 'doctor5@ya.ru',
            ],
            [
                'username' => 'user6',
                'email' => 'doctor6@ya.ru',
            ]
        ];

        foreach ($users as $value)
        {
            $user = new User();

            $user->setUsername($value['username']);
            $user->setEmail($value['email']);
            $user->setRoles(['ROLE_USER']);
            $user->setEnabled(true);
            $encoder = $this->container->get('security.password_encoder');
            $password1 = $encoder->encodePassword($user, '123');
            $user->setPassword($password1);

            $manager->persist($user);
        }

        $manager->flush();
    }
}