<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('comment', TextareaType::class, [
            'required' => false
        ])
            ->add('quality_of_food', ChoiceType::class, [
                'choices' => [
                    '5' => 5,
                    '4' => 4,
                    '3' => 3,
                    '2' => 2,
                    '1' => 1
                ],
                'expanded' => false,
                'multiple' => false,
                'attr' => [
                    'class' => 'custom_class'
                ]
            ])
            ->add('service_quality', ChoiceType::class, [
                'choices' => [
                    '5' => 5,
                    '4' => 4,
                    '3' => 3,
                    '2' => 2,
                    '1' => 1
                ],
                'expanded' => false,
                'multiple' => false,
                'attr' => [
                    'class' => 'custom_class'
                ]
            ])
            ->add('interior', ChoiceType::class, [
                'choices' => [
                    '5' => 5,
                    '4' => 4,
                    '3' => 3,
                    '2' => 2,
                    '1' => 1
                ],
                'expanded' => false,
                'multiple' => false,
                'attr' => [
                    'class' => 'custom_class'
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Submit review']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_comment_type';
    }
}
