<?php

namespace AppBundle\Repository;

/**
 * RestaurantRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RestaurantRepository extends \Doctrine\ORM\EntityRepository
{
    public function findRestaurant($data)
    {
        return $this->createQueryBuilder('r')
            ->where(
                $this->createQueryBuilder('r')->expr()->like('r.title',  ':data')
            )
            ->orWhere(
                $this->createQueryBuilder('r')->expr()->like('r.description',  ':data')
            )
            ->setParameter('data', $data)
            ->getQuery()
            ->getResult();
    }
}
